﻿--Drop TABLE [dbo].[Address]
CREATE TABLE [dbo].[Address](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255),
	[PostAddress] [varchar](255),
	[PhoneNo] [varchar](255)
PRIMARY KEY CLUSTERED 
(	
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
)
--SET IDENTITY_INSERT [dbo].[Address] ON
--INSERT INTO Address (id, Name, PostAddress, PhoneNo)
--VALUES (1, 'Name1', 'PostAddress1', 'PhoneNo1');
--INSERT INTO Address (id, Name, PostAddress, PhoneNo)
--VALUES (2, 'Name2', 'PostAddress2', 'PhoneNo2');
--INSERT INTO Address (id, Name, PostAddress, PhoneNo)
--VALUES (3, 'Name3', 'PostAddress3', 'PhoneNo3');
