﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftApplication.DataObjects
{
    /// <summary>
    /// Delivery dataobject
    /// </summary>
    public class DeliveryDO
    {
        string apiKey { get; set; }
        string booking { get; set; }
    }
}