﻿using SwiftApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftApplication.DataObjects
{
    /// <summary>
    /// Address dataobject
    /// </summary>
    public class AddressDO
    {
        public string apiKey = "3285db46-93d9-4c10-a708-c2795ae7872d";

        public Booking booking { set; get; }
    }

    /// <summary>
    /// Booking dataobject
    /// </summary>
    public class Booking
    {
        public PickupDetail pickupDetail { get; set; }
        public DropoffDetail dropoffDetail { get; set; }
    }

    /// <summary>
    /// Pickup details dataobject
    /// </summary>
    public class PickupDetail
    {
        public string address { get { return "105 collins st, 3000"; } }
    }

    /// <summary>
    /// Drop off dataobject
    /// </summary>
    public class DropoffDetail
    {
        public string address { get; set; }
    }
}
