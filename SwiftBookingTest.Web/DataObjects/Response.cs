﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftApplication.DataObjects
{
    /// <summary>
    /// Response dataobject
    /// </summary>
    public class Response
    {
        public string ResponseType { get; set; }
    }
}