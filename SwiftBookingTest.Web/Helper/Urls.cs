﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftApplication.Helper
{
    /// <summary>
    /// Urls and api
    /// </summary>
    public class Urls
    {
        public const string SwiftUrl = "https://app.getswift.co";
        public const string Quotes = "/api/v2/quotes";
        public const string Delivery = "/api/v2/deliveries";
    }
}