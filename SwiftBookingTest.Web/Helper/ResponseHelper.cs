﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;

namespace SwiftApplication.Helper
{
    /// <summary>
    ///  Response helper class
    /// </summary>
    public static class ResponseHelper
    {
        /// <summary>
        /// This method post JSON data to an Address
        /// </summary>
        /// <param name="api"> API path</param>
        /// <param name="json"> JSON data</param>
        /// <returns> Returns HTTP response </returns>
        public static string GetResponse(string api, string json)
        {
            string result = string.Empty;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(Urls.SwiftUrl);
            HttpContent contentPost = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = client.PostAsync(api, contentPost).Result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                result = response.StatusCode.ToString();
            }             

            return result;
        }

        /// <summary>
        /// This method post JSON data to an Address
        /// </summary>
        /// <param name="api"> API path</param>
        /// <param name="json"> list of JSON data</param>
        /// <returns> Returns HTTP response </returns>
        public static List<string> GetResponse(string api, List<string> json)
        {
            List<string> result = new List<string>();
            foreach (string stj in json)
                result.Add(GetResponse(api, stj));
            return result;
        }
    }
}