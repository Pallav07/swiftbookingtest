﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SwiftApplication
{
    /// <summary>
    /// Json helper
    /// </summary>
    /// <typeparam name="T"> T as class</typeparam>
    public static class JSONHelper<T> where T : class
    {
        /// <summary>
        /// Converts list of objects to JSON
        /// </summary>
        /// <param name="item"> Accepts item as parameter </param>
        /// <returns> Returns JSON string as result </returns>
        public static string CovertToJson(T item)
        {           
            string result = string.Empty;
            result = JsonConvert.SerializeObject(item);
            return result;
        }

        /// <summary>
        /// Converts list of objects to JSON
        /// </summary>
        /// <param name="list"> Accepts list as parameter </param>
        /// <returns> Returns JSON string as result </returns>
        public static List<string> CovertToJsonList(List<T> list)
        {
            List<string> result = new List<string>();
            foreach (T item in list)
                result.Add(CovertToJson(item));
            return result;
        }

    }
}