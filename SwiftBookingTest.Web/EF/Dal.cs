﻿using System.Data.Entity;
using SwiftApplication.Models;

namespace SwiftApplication.EF
{ 
    /// <summary>
    /// Data access layer
    /// </summary>
    public class Dal : DbContext
    {
        /// <summary>
        /// Dal constructor
        /// </summary>
	    public Dal() : base("DefaultConnection")
        {

        }

        /// <summary>
        /// Address DB set
        /// </summary>
        public DbSet<Address> Address { get; set; }

        /// <summary>
        /// Builds the model for the table Address
        /// </summary>
        /// <param name="modelBuilder"> Model builder </param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var v = modelBuilder.Entity<Address>().ToTable("Address");
            modelBuilder.Entity<Address>().HasKey(x => x.Id);
        }
    }
}