﻿var swiftApp = angular.module('swiftApp', []);
swiftApp.controller('addressControl', function ($scope, $http) {

    $scope.addresses = []
    $scope.submitResult = []
    $scope.submitIndividualResult = []
    $http({ method: 'GET', url: 'GetAddressDetails' }).then(function successCallback(response) {
        $scope.addresses = response.data;
        console.log("GetAddressDetailssuccessCallback");

    }, function errorCallback(response) {
        console.log("GetAddressDetailserrorCallback");
    });

    $scope.submitdata = function () {
        $http({ method: 'GET', url: 'SubmitDetails' }).then(function successCallback(response) {
            $scope.submitResult = response.data;
            console.log("SubmitDetailssuccessCallback");

        }, function errorCallback(response) {
            console.log("SubmitDetailserrorCallback");
        });
    }
    
    $scope.savedata = function (address) {
        $http({ method: 'POST', url: 'SaveAddressDetails', data: address }).then(function successCallback(response) {
            $scope.addresses = response.data;
            console.log("SaveAddressDetailssuccessCallback");

        }, function errorCallback(response) {
            console.log("SaveAddressDetailserrorCallback");
        });
    }

    $scope.submitIndividualdata = function (address) {
       
        $http({ method: 'POST', url: 'SubmitAddressDetails', data: address }).then(function successCallback(response) {
            $scope.submitIndividualResult = response.data;
            console.log(response.data);
            console.log("SubmitAddressDetailssuccessCallback");

        }, function errorCallback(response) {
            console.log("SubmitAddressDetailserrorCallback");
        });
    }

    }
);