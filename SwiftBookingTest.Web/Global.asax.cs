﻿using SwiftApplication.App_Start;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SwiftApplication
{
    public class MvcApplication : HttpApplication
    {
            protected void Application_Start()
            {
                AreaRegistration.RegisterAllAreas();
                RouteConfig.RegisterRoutes(RouteTable.Routes);
            }
    }
}

