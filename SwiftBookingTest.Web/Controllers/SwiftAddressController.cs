﻿using SwiftApplication;
using SwiftApplication.DataObjects;
using SwiftApplication.EF;
using SwiftApplication.Helper;
using SwiftApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SwiftApplication.Controllers
{
    public class SwiftAddressController : Controller
    {
        // GET: SwiftAddress
        public ActionResult Index()
        {
            return View();
        }

        // Post : SaveAddressDetails
        [HttpPost]
        public JsonResult SaveAddressDetails(Address add)
        {
            Dal dal = new Dal();
            dal.Address.Add(add);
            dal.SaveChanges();

            List<Address> addresses = dal.Address.ToList();
            return Json(addresses, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Gets JSON result from the database
        /// </summary>
        /// <returns> Returns database result in JSON format</returns>
        public JsonResult GetAddressDetails()
        {
            Dal dal = new Dal();
            List<Address> addresses = dal.Address.ToList();
            return Json(addresses, JsonRequestBehavior.AllowGet);
        }

        // Post : SubmitAddressDetails
        [HttpPost]
        public JsonResult SubmitAddressDetails(Address address)
        {
            List<Address> lst = new List<Address>();
            lst.Add(address);
            List<AddressDO> addressDO = new List<AddressDO>();
            lst.ForEach(p =>
            {
                AddressDO ado = new AddressDO();
                ado.booking = new Booking();
                ado.booking.dropoffDetail = new DropoffDetail();
                ado.booking.pickupDetail = new PickupDetail();
                ado.booking.dropoffDetail.address = p.PostAddress;
                addressDO.Add(ado);
            }
            );
            List<string> jsonList = JSONHelper<AddressDO>.CovertToJsonList(addressDO);
            List<string> hr = ResponseHelper.GetResponse(Urls.Quotes, jsonList);
            List<Response> responses = new List<Response>();
            hr.ForEach(p => responses.Add(new Response() { ResponseType = p }));
            return Json(responses, JsonRequestBehavior.AllowGet);
        }
    }
}