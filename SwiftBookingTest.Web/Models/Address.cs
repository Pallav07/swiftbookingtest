﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwiftApplication.Models
{

    public class Address
    {
        public int Id { get; internal set; }
        public string Name { get; set; }
        public string PostAddress { get; set; }
        public string PhoneNo { get; set; }
    }
}